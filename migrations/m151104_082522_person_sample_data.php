<?php

use yii\db\Schema;
use yii\db\Migration;

class m151104_082522_person_sample_data extends Migration
{
    public function up()
    {
        $this->insert('person', ['name'=>'Ronald Kasagga', 'phone'=>'0791501840']);
        $this->insert('person', ['name'=>'Benjamin Kooma', 'phone'=>'0792456917']);
    }

    public function down()
    {
        $this->delete('person');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
