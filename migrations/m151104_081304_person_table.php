<?php

use yii\db\Schema;
use yii\db\Migration;

class m151104_081304_person_table extends Migration
{
    public function up()
    {
        $this->createTable('person', [
            'id'=>Schema::TYPE_PK,
            'name'=>Schema::TYPE_STRING. ' NOT NULL',
            'phone'=>Schema::TYPE_STRING. ' NOT NULL',
            'added_on'=>Schema::TYPE_TIMESTAMP. ' NOT NULL DEFAULT CURRENT_TIMESTAMP',
            'updated_on'=>Schema::TYPE_DATETIME,
        ]);
    }

    public function down()
    {
        $this->dropTable('person');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
