<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 11/4/2015
 * Time: 11:20 AM
 */

namespace app\modules\api\versions\v1\controllers;


use yii\rest\ActiveController;

class PersonsController extends ActiveController
{
    public $modelClass = 'app\modules\api\versions\v1\models\Person';
}