<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 11/4/2015
 * Time: 10:20 AM
 */

namespace app\modules\api\versions\v1\controllers;

use yii\web\Controller;

class DefaultController extends Controller
{
    public function actionIndex(){
        return $this->render('index');
    }
}