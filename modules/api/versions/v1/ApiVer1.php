<?php

/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 11/4/2015
 * Time: 10:16 AM
 */
namespace app\modules\api\versions\v1;

use yii\base\Module;

class ApiVer1 extends Module
{
    public $controllerNamespace = 'app\modules\api\versions\v1\controllers';

    public function init(){
        parent::init();
    }
}