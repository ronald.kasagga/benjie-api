Benjie API
============================

This is a pet project written in Yii2 (basic template) demonstrating how to build a restful API. Once setup navigate to the [API page](http://localhost/benjie-api/web/api/)
and [API Version 1](http://localhost/benjie-api/web/api/v1) page to get a feel of where we are going

DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      models/             contains model classes
      modules/
            api/
                versions/
                    v1/
                        controllers/
                        ..
                    v2/
                        controllers/
                        models/
                        ...
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources



REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.4.0.

Your web server should also have mod-rewrite enabled for an ideal restful API experience